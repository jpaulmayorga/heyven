import express, { Request, Response } from 'express';

import { OrganizationsRepository } from './Repository';

const route = express.Router();

route.get('/organizations', async (_req: Request, res: Response) => {
  const repository = new OrganizationsRepository({
    tableName: 'tabla de organizaciones',
  });
  const organizations = await repository.getMany();
  res.status(200).json({ data: organizations });
});

route.get('/organizations/:id', async (req: Request, res: Response) => {
  res.status(200).json({ data: `organizations data ${req.params.id}!` });
});

export { route as organizationsApi };
