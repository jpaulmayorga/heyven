import { DocumentClient } from 'aws-sdk/clients/dynamodb';

import { Organization } from './interface';

export class OrganizationsRepository {
  tableName: string;
  private db: DocumentClient;

  constructor(options: { tableName: string }) {
    this.tableName = options.tableName;
    this.db = new DocumentClient();
  }

  async create(organization: Organization) {
    // tslint:disable-next-line: no-console
    console.log(organization);
    return 'create Ong';
  }

  async getOne(options: { id: string }) {
    // tslint:disable-next-line: no-console
    console.log(options.id);
    return 'create Ong';
  }

  async updateOne(options: { id: string; ong: string }) {
    // tslint:disable-next-line: no-console
    console.log(options);
    return 'create Ong';
  }

  async getMany() {
    // tslint:disable-next-line: no-console
    console.log(this.db);
    const dataOfExample = [
      {
        name: 'Organization 1',
      },
      {
        name: 'Organization 2',
      },
      {
        name: 'Organization 3',
      },
    ];
    // const query = await this.db
    //   .scan({
    //     TableName: this.tableName,
    //   })
    //   .promise();
    // return query.Items;
    return dataOfExample;
  }
}
