export interface Organization {
  name: string;
  logo?: string;
  objective: string;
  communityWorkWith: string[];
  description: string;
  howUseDontaions: string;
  location: string;
  sponsors?: string;
  paymentsLink?: string;
  accounts?: string;
  instructionsToDeliverProducts?: string;
  website?: string;
  email: string;
  phones: string | string[];
  whatsapp?: string;
  instagram?: string;
  facebook?: string;
  twitter?: string;
  linkedin?: string;
}
