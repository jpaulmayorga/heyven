import bodyParser from 'body-parser';
import express from 'express';
import methodOverride from 'method-override';

import { organizationsApi } from './api/organizations';
import { logRequest } from './logs/log-request';

export function init() {
  const app = express();

  /* Server Configurations */
  app.use(bodyParser.json());
  app.use(
    bodyParser.urlencoded({
      extended: false,
    }),
  );
  app.use(methodOverride());
  app.use(logRequest);

  /* Server Routes */
  app.use(organizationsApi);

  return app;
}

if (require.main === module) {
  // called directly i.e. "node app"
  init().listen(3000, (err: Error) => {
    if (err) {
      // tslint:disable-next-line: no-console
      console.error(err);
    }
    // tslint:disable-next-line: no-console
    console.log('server listening on 3000');
  });
} else {
  // required as a module => executed on aws lambda
}
