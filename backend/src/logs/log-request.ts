import { NextFunction, Request, Response } from 'express';

export async function logRequest(req: Request, _res: Response, next: NextFunction) {
  // tslint:disable-next-line: no-console
  console.log('request:', `${req.method} ${req.url}`);
  next();
}
